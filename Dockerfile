From Python:3.6

Copy . /app 

Workdir /app

Run pip install - r requirements.txt

Expose 5000

Entrypoint ["python"]

CMD ["app.py"] 
